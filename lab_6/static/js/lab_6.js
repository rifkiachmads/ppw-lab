// Calculator

var erase = false;

var go = function(x) {
  var print = document.getElementById('print');
  if (x === 'ac') {
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x==='log' || x === 'sin' || x ==='tan'){
    switch (x) {
      case 'log':
        print.value = Math.log10(print.value);
        break;
      case 'sin':
        print.value = Math.sin(print.value);
        break;
      case 'tan':
        print.value = Math.tan(print.value);
        break;
    }
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

function applyTheme(theme){
  $('body').css('background', theme.bcgColor);
  $('html').css('color', theme.fontColor);
}

if (typeof(Storage) !== "undefined") {
  if (localStorage.getItem("themes") === null || localStorage.getItem("selectedTheme") === null ) {
    localStorage.themes = `[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ]`
    localStorage.selectedTheme = JSON.stringify({"bcgColor":"#3F51B5","fontColor":"#FAFAFA"});
  }
  applyTheme(JSON.parse(localStorage.selectedTheme));
} else {
  alert("Your browser doesn't support web storage!");
}


$(document).ready(function(){
  applyTheme(JSON.parse(localStorage.selectedTheme));

  $('img[title="Expand Arrow"]').on('click', ()  => {
    $('.chat-body').toggle();
    $('img[title="Expand Arrow"]').toggleClass('rotate');
  });

  $('.chat-text').on('keypress', (event) => {
    if(event.which == 13){
      var text = $('textarea').val();
       $('textarea').val("");
       $('.msg-insert').append(`<p class="msg-send">${text}<p>`);
    }
  });

/*SELECT2*/
  $('.my-select').select2({
      'data': JSON.parse(localStorage.themes)
  });

  $('.apply-button').on('click', () => {
    var themeid = parseInt($('.my-select').select2('data')[0].id);
    localStorage.selectedTheme = JSON.stringify(JSON.parse(localStorage.themes)[themeid]);
    applyTheme(JSON.parse(localStorage.selectedTheme));
  });

});
