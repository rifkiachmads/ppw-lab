from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

class Lab6UnitTest(TestCase):
    def test_lab_6_url_exists(self):
        response = Client().get('/lab-6/')
        self.assertEquals(response.status_code, 200)

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-6/')
        self.assertEquals(found.func, index)
