from django.test import TestCase, Client
from django.urls import resolve
from .views import *


class Lab8UnitTest(TestCase):
    def test_lab_8_url_exist(self):
        response = Client().get('/lab-8/')
        self.assertEquals(response.status_code, 200)

    def test_lab_8_using_index_func(self):
        found = resolve('/lab-8/')
        self.assertEquals(found.func, index)

    def test_lab_8_using_index_template(self):
        response = Client().get('/lab-8/')
        self.assertTemplateUsed(response, 'lab_8/lab_8.html')
